FROM node:lts-alpine
 
WORKDIR /user/src/app
 
COPY . .
 
RUN npm ci
 
RUN npm run build

CMD ["npm", "run", "start:prod"]