import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { RostrumService } from './providers/rostrum.service';
import { ConfigModule } from '@nestjs/config';
import { WalletService } from './providers/wallet.service';
import { CacheService } from './providers/cache.service';
import { CacheModule } from '@nestjs/cache-manager';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSourceParams } from './data-source';
import { DBService } from './providers/db.service';
import { WalletIndex } from './entities/wallet-index.entity';
import { TokenTxState } from './entities/token-tx-state.entity';
import { HttpModule } from '@nestjs/axios';
import { TokenService } from './providers/token.service';
import { ScheduleModule } from '@nestjs/schedule';
import { SchedulerService } from './providers/scheduler.service';

@Module({
  imports: [
    ConfigModule.forRoot(), 
    CacheModule.register(),
    TypeOrmModule.forRoot(DataSourceParams),
    TypeOrmModule.forFeature([WalletIndex, TokenTxState]),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [
    CacheService,
    DBService,
    RostrumService,
    {
      inject: [RostrumService],
      provide: 'ROSTRUM',
      useFactory: async (rostrumService: RostrumService) => {
        return await rostrumService.initConnection();
      },
    },
    WalletService,
    {
      inject: [WalletService],
      provide: 'WALLETSERVICE',
      useFactory: async (walletService: WalletService) => {
        return await walletService.initKeys();
      },
    },
    TokenService,
    SchedulerService,
  ]
})
export class AppModule {}
