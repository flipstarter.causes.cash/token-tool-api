import { Injectable, Logger, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

@Injectable()
export class CacheService {

    private readonly logger = new Logger(CacheService.name);

    constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

    public async getValue<T>(key: string) {
        return await this.cacheManager.get<T>(key);
    }

    public async setValue<T>(key: string, data: T, seconds?: number) {
        let time = seconds ? seconds * 1000 : 0;
        await this.cacheManager.set(key, data, time);
    }
}
