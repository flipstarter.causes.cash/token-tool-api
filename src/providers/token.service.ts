import { HttpService } from "@nestjs/axios";
import { Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { AxiosInstance, AxiosResponse } from "axios";
import NexCore from "nexcore-lib";
import HDPrivateKey from "nexcore-lib/types/lib/hdprivatekey";
import { DerivePath, WalletHelper } from "src/helpers/wallet.helper";
import { ReentrantLock } from "src/utils/reentrant-lock";
import { DBService } from "./db.service";
import { CreateTokenDto } from "src/dtos/create-token.dto";
import { TokenTxState } from "src/entities/token-tx-state.entity";
import { randomUUID } from "crypto";
import { Utils } from "src/utils/utils";
import { WalletService } from "./wallet.service";
import { InputUTXO } from "src/interfaces/utxo.interface";
import { RostrumService } from "./rostrum.service";
import { WalletIndexColumn } from "src/entities/wallet-index.entity";

@Injectable()
export class TokenService {

  private readonly logger = new Logger(TokenService.name);

  private readonly addressLock = new ReentrantLock();
  private readonly axios: AxiosInstance;
  private readonly accountKey: HDPrivateKey;

  public constructor(
    private configService: ConfigService,
    private dbService: DBService,
    private walletService: WalletService,
    private rostrumService: RostrumService,
    private readonly httpService: HttpService
  ) {
    this.axios = httpService.axiosRef;
    this.accountKey = WalletHelper.generateAccountKey(configService.get("SEED"), 1);
  }

  public async getUnusedAddressAndKey() {
    try {
      await this.addressLock.lock();
      let idx = await this.dbService.getWalletIndexes();
      let newKey = WalletHelper.generateKeyAndAddress(this.accountKey, DerivePath.RECEIVE, idx.tokenIndex);
      await this.dbService.incrementWalletIndex(WalletIndexColumn.TOKEN_INDEX);
      return newKey;
    } finally {
      this.addressLock.unlock();
    }
  }

  public async getJsonDoc(url: string) {
    let res: AxiosResponse<string, any>;
    try {
      res = await this.axios.get<string>(url, { transformResponse: (res) => res });
    } catch {
      throw new Error(`Failed to get json from: ${url}`);
    }

    try {
      let json = JSON.parse(res.data);
      return { json: json, jsonStr: res.data };
    } catch (e) {
      throw new Error(`Failed to parse json: ${e.message}`);
    }
  }

  public async createTokenTxStateEntry(createTokenDto: CreateTokenDto, docUrl: string, docHash: string) {
    let state = new TokenTxState();
    let uuid = randomUUID();
    state.uuid = uuid;
    state.createTxId = `create_id_${uuid}`;
    state.createTxIdem = `create_idem_${uuid}`;
    state.mintTxId = `mint_id_${uuid}`;
    state.mintTxIdem = `mint_idem_${uuid}`;
    state.tokenName = createTokenDto.name;
    state.tokenTicker = createTokenDto.ticker;
    
    if (docUrl) {
      state.tokenDocUrl = docUrl;
      state.tokenDocHash = docHash;
    }

    state.tokenQuantity = createTokenDto.quantity;
    state.tokenDecimals = createTokenDto.decimals;
    state.burnAuth = createTokenDto.lockAmount;
    state.destAddress = createTokenDto.destAddress;
    state.payAddress = await this.walletService.getUnusedAddress();

    return await this.dbService.saveTokenTxState(state);
  }

  public async generataeCreateTokenTransaction(inputs: InputUTXO[], toAddr: string, changeAddr: string, ticker: string, name: string, docUrl?: string, docHash?: string, decimals?: number) {
    let opReturn = NexCore.GroupToken.buildTokenDescScript(ticker, name, docUrl, docHash, decimals);
    
    let tx = new NexCore.Transaction();

    let inputOutpoint = Buffer.from(inputs[0].outpoint_hash, 'hex');

    let id = NexCore.GroupToken.findGroupId(inputOutpoint, opReturn.toBuffer(), NexCore.GroupToken.authFlags.ACTIVE_FLAG_BITS);
    let amount = NexCore.GroupToken.getAmountBuffer(NexCore.GroupToken.authFlags.ACTIVE_FLAG_BITS | id.nonce);

    let blockTip = await this.rostrumService.getBlockTip();

    inputs.forEach(input => {
      tx.from({
        txId: input.outpoint_hash,
        outputIndex: input.tx_pos,
        script: NexCore.Script.fromAddress(input.address),
        satoshis: input.value
      });
    });
    tx.addGroupData(opReturn).toGrouped(toAddr, id.hashBuffer, amount).change(changeAddr).lockUntilBlockHeight(blockTip.height).sign(inputs[0].privKey);
    
    return { tx: tx, tokenIdBuf: id.hashBuffer };
  }

  public async generataeMintTokenTransaction(state: TokenTxState, inputs: InputUTXO[], tokenIdBuf: Buffer, changeAddr: string) {    
    let tx = new NexCore.Transaction();

    let amount = BigInt(state.tokenQuantity) * (10n ** BigInt(state.tokenDecimals));
    let tokenAmount = NexCore.GroupToken.getAmountBuffer(amount);

    let blockTip = await this.rostrumService.getBlockTip();

    inputs.forEach(input => {
      tx.from({
        txId: input.outpoint_hash,
        outputIndex: input.tx_pos,
        script: NexCore.Script.fromAddress(input.address),
        satoshis: input.value
      });
    });
    tx.toGrouped(state.destAddress, tokenIdBuf, tokenAmount);

    if (!state.burnAuth) {
      let authAmount = NexCore.GroupToken.getAmountBuffer(NexCore.GroupToken.authFlags.ACTIVE_FLAG_BITS);
      tx.toGrouped(state.destAddress, tokenIdBuf, authAmount);
    }
    
    let privKeys = inputs.map(input => input.privKey);
    tx.change(changeAddr).lockUntilBlockHeight(blockTip.height).sign(privKeys);
    
    return tx;
  }

  public async getTokenTxState(uid: string) {
    return await this.dbService.getTokenTxState(uid);
  }
}