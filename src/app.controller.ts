import { BadRequestException, Body, Controller, Get, NotFoundException, Param, Post } from '@nestjs/common';
import { CreateTokenDto, CreateTokenResponse, StatusResponse, ValidateTokenResponse } from './dtos/create-token.dto';
import { TokenHelper } from './helpers/token.helper';
import { TokenService } from './providers/token.service';
import { WalletService } from './providers/wallet.service';
import { Utils } from './utils/utils';
import { TokenTxStatus } from './entities/token-tx-state.entity';

@Controller('api')
export class AppController {

  constructor(private walletService: WalletService, private tokenService: TokenService) {}

  @Get('balance')
  async getBalance() {
    return await this.walletService.getWalletBalance();
  }

  @Get('addresses')
  async getAddresses() {
    return await this.walletService.listAddresses();
  }

  @Post('validate')
  async validateCreateToken(@Body() createTokenDto: CreateTokenDto) {
    let res: ValidateTokenResponse = { valid: false };
    let error = TokenHelper.validateTokenRequest(createTokenDto);
    if (error) {
      res.error = error;
      throw new BadRequestException(res);
    }

    if (!Utils.isNullOrWhitespace(createTokenDto.docUrl)) {
      try {
        let json = await this.tokenService.getJsonDoc(createTokenDto.docUrl);
        res.jsonStr = json.jsonStr;
      } catch (e) {
        res.error = e.message;
        throw new BadRequestException(res);
      }
    }

    res.valid = true;
    return res;
  }

  @Post('create')
  async createToken(@Body() createTokenDto: CreateTokenDto) {
    let error = TokenHelper.validateTokenRequest(createTokenDto);
    if (error) {
      throw new BadRequestException(error);
    }

    let docUrl: string = null; 
    let docHash: string = null;
    if (!Utils.isNullOrWhitespace(createTokenDto.docUrl)) {
      docUrl = createTokenDto.docUrl;
      try {
        let json = await this.tokenService.getJsonDoc(createTokenDto.docUrl);
        docHash = TokenHelper.calcJsonDocHash(json.jsonStr);
      } catch (e) {
        throw new BadRequestException(e.message);
      }
    }

    let state = await this.tokenService.createTokenTxStateEntry(createTokenDto, docUrl, docHash);
    return { uid: state.uuid, status: TokenTxStatus.PENDING, payAddress: state.payAddress, payAmount: TokenHelper.PAYMENT_AMOUNT, payUrl: `${state.payAddress}?amount=${TokenHelper.PAYMENT_AMOUNT}.00` } as CreateTokenResponse;
  }

  @Get('status/:uid')
  async getStatus(@Param('uid') uid: string) {
    let state = await this.tokenService.getTokenTxState(uid);
    if (!state) {
      throw new NotFoundException(`Process with id: ${uid} not found.`);
    }

    if (state.status === TokenTxStatus.PENDING) {
      return { uid: uid, status: state.status, payAddress: state.payAddress, payAmount: TokenHelper.PAYMENT_AMOUNT, payUrl: `${state.payAddress}?amount=${TokenHelper.PAYMENT_AMOUNT}.00` }
    }

    if (state.status !== TokenTxStatus.DONE) {
      return { uid: uid, status: state.status }
    }

    let res = { uid: uid, status: state.status, createTxIdem: state.createTxIdem, mintTxIdem: state.mintTxIdem, tokenId: state.tokenId, jsonSignature: "None" } as StatusResponse;
    if (state.tokenDocSignature) {
      res.jsonSignature = state.tokenDocSignature;
    }
    return res;
  }
}
