import { TokenTxStatus } from "src/entities/token-tx-state.entity";

export class CreateTokenDto {
    name: string;
    ticker: string;
    docUrl: string;
    decimals: number;
    quantity: string;
    lockAmount: boolean;
    destAddress: string;
}

export interface CreateTokenResponse {
    uid: string;
    status: TokenTxStatus;
    payAddress: string;
    payAmount: number;
    payUrl: string;
}

export interface ValidateTokenResponse {
    valid: boolean;
    jsonStr?: string;
    error?: string;
}

export interface StatusResponse {
    uid: string;
    status: TokenTxStatus;
    createTxIdem: string;
    mintTxIdem: string;
    tokenId: string;
    jsonSignature: string;
}