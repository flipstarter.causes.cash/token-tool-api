import PrivateKey from "nexcore-lib/types/lib/privatekey"

export interface UnspentUTXO {
    has_token: boolean,
    height: number,
    outpoint_hash: string,
    tx_hash: string,
    tx_pos: number,
    value: number
}

export interface SpentUTXO {
    height: number,
    tx_hash: string,
    tx_pos: number,
}

export interface UTXO {
    addresses: string[],
    amount: number,
    group: string,
    group_authority: number | bigint | string,
    group_quantity: number | bigint | string,
    height: number,
    scripthash: string,
    scriptpubkey: string,
    spent: SpentUTXO,
    status: string,
    template_argumenthash: string,
    template_scripthash: string,
    token_id_hex: string,
    tx_hash: string,
    tx_idem: string,
    tx_pos: number
}

export interface InputUTXO {
    outpoint_hash: string,
    tx_pos: number,
    value: number,
    address: string,
    privKey: PrivateKey
}
