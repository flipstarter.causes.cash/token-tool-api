import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, Index } from 'typeorm';

export enum TokenTxStatus {
  PENDING = 'Pending',
  CREATING = 'Creating',
  MINTING = 'Minting',
  DONE = 'Done',
  CANCELED = 'Canceled',
  FAILED = 'Failed'
}

@Entity()
export class TokenTxState {

  @PrimaryGeneratedColumn()
  id: number

  @Column({ unique: true, generated: 'uuid' })
  uuid: string;

  @Column({ unique: true })
  createTxId: string;

  @Column({ unique: true })
  createTxIdem: string;

  @Column({ unique: true })
  mintTxId: string

  @Column({ unique: true })
  mintTxIdem: string

  @Column()
  tokenName: string;

  @Column()
  tokenTicker: string;

  @Column({ nullable: true })
  tokenDocUrl: string;

  @Column({ nullable: true })
  tokenDocHash: string;

  @Column({ nullable: true })
  tokenDocSignature: string;

  @Column({ nullable: true })
  tokenDecimals: number;

  @Column({ nullable: true })
  tokenQuantity: string;

  @Column({ nullable: true })
  tokenId: string;

  @Column()
  burnAuth: boolean;

  @Column()
  destAddress: string;

  @Column()
  payAddress: string;

  @Index()
  @Column({ default: TokenTxStatus.PENDING })
  status: TokenTxStatus;

  @CreateDateColumn()
  date: Date;
}
