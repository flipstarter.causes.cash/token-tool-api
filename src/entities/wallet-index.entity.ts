import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

export enum WalletIndexColumn {
  RECEIVE_INDEX = 'receiveIndex',
  CHANGE_INDEX = 'changeIndex',
  TOKEN_INDEX = 'tokenIndex',
}

@Entity()
export class WalletIndex {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  receiveIndex: number;

  @Column()
  changeIndex: number;

  @Column()
  tokenIndex: number;
}
