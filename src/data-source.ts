import { DataSource, DataSourceOptions } from "typeorm";
import { WalletIndex } from "./entities/wallet-index.entity";
import { TokenTxState } from "./entities/token-tx-state.entity";
import { InitDb1690475390730 } from "./migrations/1690475390730-init_db";

export const DataSourceParams: DataSourceOptions = {
    type :"sqlite",
    database: "data/walletdb.sqlite",
    entities: [WalletIndex, TokenTxState],
    synchronize: false,
    migrationsRun: true,
    migrations: [InitDb1690475390730]
}

export const AppDataSource = new DataSource(DataSourceParams);